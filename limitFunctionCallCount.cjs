function limitFunctionCallCount(cb, n) {
    let count = 0;

    return function() {
        if (count < n) {
            count++;
            return cb();
        } else {
            console.log("Function call limit reached.");
            return null; 
        }
    };
}

module.exports = limitFunctionCallCount;