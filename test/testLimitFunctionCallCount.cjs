const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

// Define a mock callback function
function mockCallback() {
   console.log("called function");
}

// Test limiting function calls
const limitedFunc = limitFunctionCallCount(mockCallback, 2);
limitedFunc();
limitedFunc();
limitedFunc();
