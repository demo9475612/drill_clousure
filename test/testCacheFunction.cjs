const cacheFunction = require('../cacheFunction.cjs');

// Test caching function calls
const cachedFunc = cacheFunction((x, y) => x + y);
console.log(cachedFunc(3, 4)); // 7
console.log(cachedFunc(3, 4)); // Retrieving result from cache. 7
console.log(cachedFunc(5, 6)); // 11
console.log(cachedFunc(3, 4)); // Retrieving result from cache. 7