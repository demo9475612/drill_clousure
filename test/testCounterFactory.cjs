const counterFactory = require('../counterFactory.cjs');


//Testing increment and Decrement methods;
const counter = counterFactory();

console.log('Calling increment function 1st time:',counter.increment());
console.log('Calling increment function 2nd time:',counter.increment());
console.log('Calling decrement function 1st time:',counter.decrement());
console.log('Calling decrement function 2st time:',counter.decrement());