function cacheFunction(cb) {
    const cache = {};

    return function (...args) {
        const key = JSON.stringify(args);
        if (cache[key]) {
            console.log("Retrieving result from cache.");
            return cache[key];
        } else {
            const result = cb(...args);
            cache[key] = result;
            return result;
        }
    };
}

module.exports = cacheFunction;