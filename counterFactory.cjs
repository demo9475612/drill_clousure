function counterFactory() {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.


    //Counter variable to store the value;
    let counterValue=0;

    // increment function to increment the counter value ;
    function increment(){
        return ++counterValue;
    }

    //Decrement function to decrement the counter value;
    function decrement(){
       
        return --counterValue;
    }
    //returning an object which having two methods;
    return {increment , decrement};

}

module.exports = counterFactory;